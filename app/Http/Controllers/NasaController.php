<?php

namespace App\Http\Controllers;

use App\Nasa;
use Illuminate\Http\Request;

class NasaController extends Controller
{
    public function __construct()
    {
        // TODO: API Authorization policy
    }

    /**
     * Reset the actual ship position
     *
     * @return \Illuminate\Http\Response
     */
    public function reset()
    {
        Nasa::reset();

        response()->json(['success' => 'success'], 200);
    }

    /**
     * Change ship position based on user direction commands
     *
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request)
    {
        $input = $request->input('movimentos');
        // TODO: validate input

        $newPosition = Nasa::getNewPosition($input);

        return response()->json($newPosition);
    }

    /**
     * Retrieve actual ship position
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status()
    {
        $position = Nasa::getActualPosition();

        return response()->json($position);
    }
}
