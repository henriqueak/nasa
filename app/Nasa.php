<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nasa extends Model
{
    protected $table = 'nasa';

    protected $primaryKey = 'id';

    protected $fillable = [
        'direction',
        'xcoordinate',
        'ycoordinate'
    ];

    public $timestamps = false;

    /**
     * Translate integer position into human readable string
     *
     * @param $pos
     * @return string|null
     */
    private static function positionToString($pos) {
        switch($pos) {
            case 0:
                return "N"; // North
            case 1:
                return "E"; // East
            case 2:
                return "S"; // South
            case 3:
                return "W"; // West
        }

        return null;
    }

    /**
     * Check if after a move, ship is still inside bounds
     *
     * @param $position
     * @return bool
     */
    private static function isInsideBounds($position) {
        $minBound = 0;
        $maxBound = 5;

        if ($position[0] < $minBound || $position[0] > $maxBound ||
                $position[1] < $minBound || $position[1] > $maxBound) {
            return false;
        }

        return true;
    }

    /**
     * Increment part of descriptive ship movements
     * TODO: throw away Facade with static functions and made parameters as class members
     */
    private static function incrementDescription($description, $count, $horizontal) {
        $description .= " andou $count casas no eixo ";
        if ($horizontal)
            $description .= "x,";
        else
            $description .= "y,";

        return $description;
    }

    /**
     * Get descriptive movements done by the ship accordingly user data
     *
     * @param $movimentos
     * @return string
     */
    private static function getMovimentDescription($movimentos) {
        $description = 'A sonda';
        // we can put this horizontal/vertical logic into another class to clarify and isolate
        $horizontal = true;
        $count = 0;

        for ($i = 0; $i < count($movimentos); $i++) {
            if($movimentos[$i] == "GE") {
                if($count > 0)
                    $description = self::incrementDescription($description, $count, $horizontal);

                $description .= " virou para a esquerda,";
                $count = 0;
                $horizontal = !$horizontal;
            } elseif ($movimentos[$i] == "GD") {
                if($count > 0)
                    $description = self::incrementDescription($description, $count, $horizontal);

                $description .= " virou para a direita,";
                $count = 0;
                $horizontal = !$horizontal;
            } elseif ($movimentos[$i] == "M") {
                $count++;
            }
        }

        if($count > 0) {
            $description .= " andou $count casas no eixo ";
            if ($horizontal)
                $description .= "x,";
            else
                $description .= "y,";
        }

        return $description;
    }

    /**
     * Retrieve the actual ship position
     *
     * @return array
     */
    public static function getActualPosition() {
        $actualPosition = Nasa::first();
        return (["x" => $actualPosition->xcoordinate, "y" => $actualPosition->ycoordinate,
            "face" => self::positionToString($actualPosition->direction)]);
    }

    /**
     * Reset the actual ship position
     */
    public static function reset() {
        Nasa::first()->update(['direction' => 1, 'xcoordinate' => 0, 'ycoordinate' => 0]);
    }

    /**
     * Move the ship accordingly user data
     *
     * @param $movimentos
     * @return array
     */
    public static function getNewPosition($movimentos) {
        // retrieve last position (moves are cumulative between API calls)
        $actualPosition = Nasa::first();
        $direction_index = $actualPosition->direction;
        $position = [$actualPosition->xcoordinate, $actualPosition->ycoordinate];

        // move ship accordingly user data
        foreach($movimentos as $mov) {
            if ($mov == 'GE') {
                $direction_index = abs(($direction_index - 1)) % 4;
            } elseif ($mov == 'GD') {
                $direction_index = abs(($direction_index + 1)) % 4;
            }elseif ($mov == 'M') {
                switch ($direction_index) {
                    case 0:
                        $position[1]++;
                        break;
                    case 1:
                        $position[0]++;
                        break;
                    case 2:
                        $position[1]--;
                        break;
                    case 3:
                        $position[0]--;
                        break;
                }

                // validate position: must be between 0 - 5, inclusive
                if(!self::isInsideBounds($position)) {
                    return ["erro" => "Movimento para fora dos limites"];
                }
            }
        }

        // Save actual position
        $actualPosition->update(['direction' => $direction_index, 'xcoordinate' => $position[0], 'ycoordinate' => $position[1]]);

        // Get moviment description
        $description = self::getMovimentDescription($movimentos);

        return (['x' => $position[0], 'y' => $position[1], 'description' => $description]);
    }
}
