# NASA SHIP API

This is a little test API that controls and moves a ship on a 5x5 board.


# System Requeriments

In order to use this app you need:
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- BCMath PHP Extension
- MySQL or MariaDB

## Clone or download our repository
Just clone this repository to your webserver.


## Prepare a database
Setup the database before starting the application. You can set any name you want for this simple database.
Edit the project .env file, setting your specific information:
```
- DB_CONNECTION=mysql  
- DB_HOST=127.0.0.1  
- DB_PORT=3306  
- DB_DATABASE=dbname 
- DB_USERNAME=dbuser
- DB_PASSWORD=dbpassword
```
Just be sure that you have an already existing empty database named as your DB_DATABASE setting.

## Setup the database
Now, we can create the DB tables. At the top of directory application, run onto a command line:
```
$ php artisan migrate
```
## Feed DB with initial data
The following command will just put some initial values on *nasa* table for (x,y) coordinates and set the initial ship face directed to *East*.
```
$ php artisan db:seed
```
# Note
All the above commands can be unified into a little one script (deploy like).
We could have used a single flat text file to store this data...

## Run the app locally
You do not need to have an Apache or NGinx pre-installed and running to test this application. Neither to run the unit tests.
Just with installed PHP components you can run the built-in webserver:
```
$ php -S localhost:8080 -t ./public
```
Just be sure to run the command at app top directory (maybe */home/user/nasa*) and port **8080** is free (you can choose any high level port).

# Endpoints
After locally running the app, you may test using **curl** app:
### Status endpoint
```
$ curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/status
```
### Change endpoint
Observe that this API call is cumulative. If you do not reset the actual ship position, a new call will continue from the last changed position.
```
$ curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"movimentos":["GE","M","M","M","GD","M","M"]}' \
  http://localhost:8080/api/change
```

### Reset endpoint
```
$ curl --header "Content-Type: application/json" \
  --request PUT \
  http://localhost:8080/api/reset
```

# Unit Tests
To run the unit tests, execute at the app root directory:
```
$ ./vendor/bin/phpunit
```

The written test just cover some routing tests. They do not cover Class level tests.


# Hardway deploy
If you have a server running some webser application like *Apache* or *NGinx* and some other running *MySQL*, you can **scp** or clone the repository files to your document root.
Create a VirtualHost (Apache) and point the public directory to our *nasa/public* dir.
If your MySQL instance isn't running locally, you can point the IP of MySQL server at *.env* file.
Use *.env* only for testing purposes.
