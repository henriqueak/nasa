<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NasaTest extends TestCase
{

    /**
     * Test reset route.
     *
     * @return void
     */
    public function testReset()
    {
        $response = $this->call('PUT', 'api/reset');

        $response->assertStatus(200);
    }

    /**
     * Test change route.
     *
     * @return void
     */
    public function testChange()
    {
        $response = $this->call('POST', 'api/change',  ['movimentos' => ["GE", "M", "M", "M", "GD", "M", "M"]]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'x' => 2,
                'y' => 3,
                'description' => 'A sonda virou para a esquerda, andou 3 casas no eixo y, virou para a direita, andou 2 casas no eixo x,',
            ]);
    }

    /**
     * Test change route.
     *
     * @return void
     */
    public function testChangeSequencial()
    {
        $response = $this->call('POST', 'api/change',  ['movimentos' => ["M","GE", "M"]]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'x' => 3,
                'y' => 4,
                'description' => 'A sonda andou 1 casas no eixo x, virou para a esquerda, andou 1 casas no eixo y,',
            ]);
    }

    /**
     * Test change route.
     *
     * @return void
     */
    public function testChangeOutOfBound()
    {
        $this->call('PUT', 'api/reset');
        $response = $this->call('POST', 'api/change',  ['movimentos' => ["GD", "M"]]);

        $response
            ->assertStatus(200)
            ->assertJson(['erro' => 'Movimento para fora dos limites']);
    }

    /**
     * Testing status route.
     *
     * @return void
     */
    public function testStatus()
    {
        $this->call('PUT', 'api/reset');
        $response = $this->call('GET', 'api/status');

        $response
            ->assertStatus(200)
            ->assertJson([
                'x' => 0,
                'y' => 0,
                'face' => 'E',
            ]);
    }
}
