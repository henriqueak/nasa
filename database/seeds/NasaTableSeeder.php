<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class NasaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nasa')->insert([
            'direction'   => 1,
            'xcoordinate' => 0,
            'ycoordinate' => 0
        ]);
    }
}
